from sqlalchemy import Column, DateTime, func, String, Numeric, Integer

from models.base import Base


class Stock(Base):
    __tablename__ = 'stocks'
    stock = Column(String(50), primary_key=True)
    low = Column(Numeric(12, 2))
    high = Column(Numeric(12, 2))
    open = Column(Numeric(12, 2))
    close = Column(Numeric(12, 2))
    date = Column(DateTime(), primary_key=True)
    volume = Column(Numeric(12, 2))
    time = Column(Integer)
    day = Column(Integer)
    pass
