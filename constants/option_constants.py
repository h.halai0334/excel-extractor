ACTION_ARG_SHORT = '-a'
ACTION_ARG_LONG = '--action'

ACTION_TYPE_IMPORT = 'import'
ACTION_TYPE_LOAD = 'load'

DIRECTORY_ARG_SHORT = '-d'
DIRECTORY_ARG_LONG = '--directory'

START_1_ARG_LONG = '--start1'
START_2_ARG_LONG = '--start2'
END_1_ARG_LONG = '--end1'
END_2_ARG_LONG = '--end2'

ALL_ARG_SHORT = ':'.join([ACTION_ARG_SHORT, DIRECTORY_ARG_SHORT]) + ':'
ALL_ARG_LONG = [argv.strip('-') + '=' for argv in [ACTION_ARG_LONG, DIRECTORY_ARG_LONG,
                                                   START_1_ARG_LONG,
                                                   START_2_ARG_LONG,
                                                   END_1_ARG_LONG,
                                                   END_2_ARG_LONG,
                                                   ]]
