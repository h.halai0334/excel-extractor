import glob
import os
import sys
import tkinter as tk
import tkinter.ttk as ttk
from multiprocessing import Process
from timeit import default_timer as timer
from tkinter import filedialog, END, messagebox

import pandas as pd
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker

from helpers.config_helper import ConfigHelper
from helpers.csv_helper import CsvHelper
from helpers.excel_helper import ExcelHelper
from models import base
from models.stock import Stock

sqlquery = """SELECT
    CAST ( [date] AS DATE ) as 'Date',
    [open] as 'Open',
    MAX(high) AS High,
    MIN(low) AS Low,
		Sum(volume) as Volume,
    [close] as 'Close'
FROM
(
    SELECT
			FIRST_VALUE ( [open] ) OVER ( PARTITION BY CAST ( [date] AS DATE ) ORDER BY [time] ) AS [open],
		FIRST_VALUE ( [close] ) OVER ( PARTITION BY CAST ( [date] AS DATE ) ORDER BY [time] DESC ) AS [close],
	[date],
	low,
	high,
	volume
FROM
	stocks
		WHERE stock = '{}' And [time] >= {} And [time] <= {}
) as T
GROUP BY CAST ( [date] AS DATE ), [open], [close]
ORDER BY CAST ( [date] AS DATE )"""


def select_directory():
    directory_path = filedialog.askdirectory(title='Select Source Directory')
    txt_directory.configure(state='normal')
    txt_directory.delete(1.0, END)
    txt_directory.insert(END, directory_path)
    txt_directory.configure(state='disabled')
    pass


def insert_to_db(connection: str, directory, file):
    print(f'Reading file {file}')
    rows = csv_helper.read_all_rows(os.path.join(directory, file))
    print(f'Reading file {file} Finished')
    print(f'{file} Found Rows: {len(rows)}')
    print(f'{file} Checking Database for existing rows')
    row = rows[0]
    l_engine = create_engine(connection)
    with l_engine.connect() as con:
        db_session = sessionmaker(con)()
        db_stock = db_session.query(Stock).filter(Stock.stock == row["stock"],
                                                  Stock.date == row["date"]).first()
        if db_stock is not None:
            print(f'{file} Skipping Rows: {len(rows)} as its already in Database')
            return
        else:
            print(f'{file} Rows not found')
        print(f'{file} Inserting Rows: {len(rows)}')
        start = timer()
        db_session.bulk_insert_mappings(Stock, rows)
        db_session.commit()
        end = timer()
        print(f'{file} Inserted Rows: {len(rows)}')
        print(f'{file} Took {end - start} Seconds')
        con.close()


pass


def select_file():
    directory_path = filedialog.askopenfilename(title='Select Excel File',
                                                filetypes=[("Excel files", ".xlsx .xls .xlsm")])
    txt_excel.configure(state='normal')
    txt_excel.delete(1.0, END)
    txt_excel.insert(END, directory_path)
    txt_excel.configure(state='disabled')
    pass


def import_data():
    try:
        btn_import.configure(state='disabled')
        directory: str = txt_directory.get(1.0, END)
        directory = directory.strip('\n')
        os.chdir(directory)
        files = glob.glob("*.txt")
        print(f'Found Files: {len(files)}')
        print(files)
        processes = []
        for file in files:
            try:
                p = Process(target=insert_to_db, args=(config.connection_string, directory, file))
                p.start()
                processes.append(p)
            except:
                print(f'Could Not insert Rows For file: {file}')
                print(f'Error: {sys.exc_info()}')
        for p in processes:
            p.join()
        print('All files processed and Added in the DB')
        messagebox.showinfo('Success', 'All files processed and Added in the DB')
    except:
        messagebox.showerror('Error', 'Error Occurred while adding')
        print(sys.exc_info())
    finally:
        btn_import.configure(state='normal')
    pass


def load_data():
    try:
        btn_load.configure(state='disabled')
        execl_file: str = txt_excel.get(1.0, END)
        execl_file = execl_file.strip('\n')
        excel_helper.get_active_instance(execl_file)
        dates = excel_helper.get_cell_value_by_name_range_vertical('H_Dates', 'A3', 'A2992')
        stock_names = excel_helper.get_cell_value_by_name_range_horizontal('H_Dates', 'B1', 'ACF1')
        index = 1
        for stock in stock_names:
            index += 1
            start1 = cbx_start1.get().split(':')
            end1 = cbx_end1.get().split(':')
            startTime = (int(start1[0]) * 60) + int(start1[1])
            endTime = (int(end1[0]) * 60) + int(end1[1])
            sql_query = text(sqlquery.format(stock, startTime, endTime))
            result = engine.execute(sql_query).fetchall()
            start2 = cbx_start2.get().split(':')
            end2 = cbx_end2.get().split(':')
            startTime = (int(start2[0]) * 60) + int(start2[1])
            endTime = (int(end2[0]) * 60) + int(end2[1])
            sql_query = text(sqlquery.format(stock, startTime, endTime))
            result2 = engine.execute(sql_query).fetchall()
            if len(result) == 0 and len(result2) == 0:
                continue
            resultWritten1 = 0
            resultWritten2 = 0
            result = [dict(row) for row in result]
            result2 = [dict(row) for row in result2]
            dateIndex = 1
            for i in range(len(dates)):
                dateIndex += 1
                if True:
                    searchDate = dates[i].date()
                    searchDate1 = searchDate + relativedelta(months=0)
                    searchDate2 = searchDate + relativedelta(months=0)
                    firstBreak = 0
                    while True:
                        try:
                            q = (row for row in result if row['Date'] == searchDate1)
                            row1 = next(q, [])
                            if len(row1) == 0:
                                if firstBreak > LIMIT:
                                    firstBreak = 0
                                    break
                                firstBreak += 1
                                searchDate1 = searchDate1 + relativedelta(days=-1)
                                continue
                            excel_helper.write_stock_data(1, dateIndex + 1, index, row1)
                            resultWritten1 += 1
                            print(f'Process 1: {resultWritten1}')
                            break
                        except StopIteration:
                            if firstBreak > LIMIT:
                                firstBreak = 0
                                break
                            firstBreak += 1
                            searchDate1 = searchDate1 + relativedelta(days=-1)
                            pass
                    firstBreak = 0
                    while True:
                        try:
                            q = (row for row in result2 if row['Date'] == searchDate2)
                            row2 = next(q, [])
                            if len(row2) == 0:
                                if firstBreak > LIMIT:
                                    firstBreak = 0
                                    break
                                firstBreak += 1
                                searchDate2 = searchDate2 + relativedelta(days=-1)
                                continue
                            excel_helper.write_stock_data(2, dateIndex + 1, index, row2)
                            resultWritten2 += 1
                            print(f'Process 2: {resultWritten2}')
                            break
                        except StopIteration:
                            if firstBreak > LIMIT:
                                firstBreak = 0
                                break
                            firstBreak += 1
                            searchDate2 = searchDate2 + relativedelta(days=-1)
                            pass
        excel_helper.save(execl_file)
        print('Loaded from DB')
        messagebox.showinfo('Success', 'Loaded from DB')
    except:
        messagebox.showerror('Error', 'Error Occurred while loading')
        print(sys.exc_info())
    finally:
        btn_load.configure(state='normal')
    pass


excel_helper = ExcelHelper()
csv_helper = CsvHelper()
config: ConfigHelper = ConfigHelper()
engine = None
LIMIT = 90

if __name__ == '__main__':
    cbx_values = [str(time)
                  for time in
                  pd.date_range("00:00", "23:55", freq="15min").time
                  if 7 < time.hour <= 20]
    print(cbx_values)
    # cbx_values = ['']
    config.read_config()
    engine = create_engine(config.connection_string)
    session = sessionmaker(engine)()
    base.Base.metadata.create_all(engine, checkfirst=True)
    root = tk.Tk()
    root.title("Excel Extractor")
    canvas = tk.Canvas(root, height=600, width=500, bg="#263D42")
    canvas.pack()

    frame = tk.Frame(root, bg="white")
    frame.place(relwidth=0.8, relheight=0.8, relx=0.1, rely=0.1)
    frame.grid_rowconfigure(0, weight=1)
    frame.grid_columnconfigure(0, weight=1)
    frame.grid_columnconfigure(1, weight=5)
    btn_directory = tk.Button(frame, text="Select Directory",
                              width=10,
                              padx=10, pady=5, fg="white", bg="#263D42", command=select_directory)
    btn_directory.grid(row=0, column=0, columnspan=2, pady=2)
    txt_directory = tk.Text(frame, width=30,
                            height=5,
                            state="disabled",
                            padx=10, pady=5)
    txt_directory.grid(row=1, column=0, columnspan=2, pady=2)
    btn_import = tk.Button(frame, text="Import",
                           width=10,
                           padx=10, pady=5, fg="white", bg="#263D42", command=import_data)
    btn_import.grid(row=2, column=0, columnspan=2, pady=2)
    txt_excel = tk.Text(frame, width=30,
                        height=5,
                        state="disabled",
                        padx=10, pady=5)
    txt_excel.grid(row=3, column=0, columnspan=2, pady=2)
    btn_select_file = tk.Button(frame, text="Select File",
                                width=10,
                                padx=10, pady=5, fg="white", bg="#263D42", command=select_file)
    btn_select_file.grid(row=4, column=0, columnspan=2, pady=2)

    lbl_start1 = tk.Label(frame, text='Start 1')
    lbl_start1.grid(row=5, column=0, pady=2)
    cbx_start1 = ttk.Combobox(frame, width=20)
    cbx_start1.grid(row=5, column=1, pady=2)
    lbl_end1 = tk.Label(frame, text='End 1')
    lbl_end1.grid(row=6, column=0, pady=2)
    cbx_end1 = ttk.Combobox(frame, width=20)
    cbx_end1.grid(row=6, column=1, pady=2)
    cbx_start1['values'] = cbx_values
    cbx_end1['values'] = cbx_values
    lbl_start2 = tk.Label(frame, text='Start 2')
    lbl_start2.grid(row=7, column=0, pady=2)
    cbx_start2 = ttk.Combobox(frame, width=20)
    cbx_start2.grid(row=7, column=1, pady=2)
    lbl_end2 = tk.Label(frame, text='End 2')
    lbl_end2.grid(row=8, column=0, pady=2)
    cbx_end2 = ttk.Combobox(frame, width=20)
    cbx_end2.grid(row=8, column=1, pady=2)
    cbx_start2['values'] = cbx_values
    cbx_end2['values'] = cbx_values
    btn_load = tk.Button(frame, text="Load",
                         width=10,
                         padx=10, pady=5, fg="white", bg="#263D42", command=load_data)
    btn_load.grid(row=9, column=0, columnspan=2)
    root.mainloop()
