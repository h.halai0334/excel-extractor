import datetime
import glob
import os
import sys
from timeit import default_timer as timer

from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker

import constants.option_constants as option
from helpers.arg_helper import ArgumentHelper
from helpers.config_helper import ConfigHelper
from helpers.csv_helper import CsvHelper
from helpers.excel_helper import ExcelHelper
from models import base
from models.stock import Stock

sqlquery = """SELECT
    CAST ( [date] AS DATE ) as 'Date',
    [open] as 'Open',
    MAX(high) AS High,
    MIN(low) AS Low,
		Sum(volume) as Volume,
    [close] as 'Close'
FROM
(
    SELECT
			FIRST_VALUE ( [open] ) OVER ( PARTITION BY CAST ( [date] AS DATE ) ORDER BY [time] ) AS [open],
		FIRST_VALUE ( [close] ) OVER ( PARTITION BY CAST ( [date] AS DATE ) ORDER BY [time] DESC ) AS [close],
	[date],
	low,
	high,
	volume
FROM
	stocks
		WHERE stock = '{}' And [time] >= {} And [time] <= {}
) as T
GROUP BY CAST ( [date] AS DATE ), [open], [close]
ORDER BY CAST ( [date] AS DATE )"""
action = ''
directory = ''
engine = None
config: ConfigHelper = ConfigHelper()
csv_helper = CsvHelper()
arg_helper = ArgumentHelper()
excel_helper = ExcelHelper()
LIMIT = 2
if __name__ == '__main__':
    config.read_config()
    engine = create_engine(config.connection_string)
    arg_helper.check_args(sys.argv[1:])
    directory = arg_helper.directory
    action = arg_helper.action
    Session = sessionmaker(engine)
    session = Session()
    base.Base.metadata.create_all(engine, checkfirst=True)
    if action == option.ACTION_TYPE_IMPORT:
        os.chdir(directory)
        files = glob.glob("*.txt")
        print(f'Found Files: {len(files)}')
        print(files)
        for file in files:
            try:
                print(f'Reading file {file}')
                rows = csv_helper.read_all_rows(os.path.join(directory, file))
                print(f'Reading file {file} Finished')
                print(f'Found Rows: {len(rows)}')
                print(f'Checking Database for existing rows')
                row = rows[0]
                dbStock = session.query(Stock).filter(Stock.stock == row["stock"], Stock.date == row["date"]).first()
                if dbStock is not None:
                    print(f'Skipping Rows: {len(rows)} as its already in Database')
                    continue
                else:
                    print('Rows not found')
                print(f'Inserting Rows: {len(rows)}')
                start = timer()
                session.bulk_insert_mappings(Stock, rows)
                session.commit()
                end = timer()
                print(f'Inserted Rows: {len(rows)}')
                print(f'Took {end - start} Seconds')  # Time in seconds, e.g. 5.38091952400282
            except:
                print(f'Could Not insert Rows For file: {file}')
                print(f'Error: {sys.exc_info()[0]}')
        print('All files processed and Added in the DB')
    elif action == option.ACTION_TYPE_LOAD:
        try:
            excel_helper.get_active_instance('')
            dates = excel_helper.get_cell_value_by_name_range('H_Dates', 'A3', 'A2992')
            stock_names = excel_helper.get_cell_value_by_name_range('H_Dates', 'B1', 'ACF1')
            index = 1
            for stock in stock_names:
                index += 1
                column_letter = excel_helper.column_string(index)
                allowedDates = excel_helper.get_cell_value_by_name_range('H_Dates', f'{column_letter}3',
                                                                         f'{column_letter}2992')
                start1 = arg_helper.start1.split(':')
                end1 = arg_helper.end1.split(':')
                startTime = (int(start1[0]) * 60) + int(start1[1])
                endTime = (int(end1[0]) * 60) + int(end1[1])
                sql_query = text(sqlquery.format(stock, startTime, endTime))
                result = engine.execute(sql_query).fetchall()
                start2 = arg_helper.start2.split(':')
                end2 = arg_helper.end2.split(':')
                startTime = (int(start1[0]) * 60) + int(start1[1])
                endTime = (int(end1[0]) * 60) + int(end1[1])
                sql_query = text(sqlquery.format(stock, startTime, endTime))
                result2 = engine.execute(sql_query).fetchall()
                if len(result) == 0 and len(result2) == 0:
                    continue
                resultWritten1 = 0
                resultWritten2 = 0
                result = [dict(row) for row in result]
                result2 = [dict(row) for row in result2]
                dateIndex = 1
                for i in range(len(dates)):
                    dateIndex += 1
                    if True:
                        searchDate = datetime.datetime.strptime(str(dates[i]), "%Y-%m-%d %H:%M:%S%z").date()
                        searchDate1 = searchDate + relativedelta(months=0)
                        searchDate2 = searchDate + relativedelta(months=0)
                        firstBreak = 0
                        while True:
                            try:
                                q = (row for row in result if row['Date'] == searchDate1)
                                row1 = next(q, [])
                                if len(row1) == 0:
                                    if firstBreak > LIMIT:
                                        firstBreak = 0
                                        break
                                    firstBreak += 1
                                    searchDate1 = searchDate1 + relativedelta(days=-1)
                                    continue
                                excel_helper.write_stock_data(1, dateIndex + 1, index, row1)
                                resultWritten1 += 1
                                print(f'Process 1: {resultWritten1}')
                                break
                            except StopIteration:
                                if firstBreak > LIMIT:
                                    firstBreak = 0
                                    break
                                firstBreak += 1
                                searchDate1 = searchDate1 + relativedelta(days=-1)
                                pass
                        firstBreak = 0
                        while True:
                            try:
                                q = (row for row in result2 if row['Date'] == searchDate2)
                                row2 = next(q, [])
                                if len(row2) == 0:
                                    if firstBreak > LIMIT:
                                        firstBreak = 0
                                        break
                                    firstBreak += 1
                                    searchDate2 = searchDate2 + relativedelta(days=-1)
                                    continue
                                excel_helper.write_stock_data(2, dateIndex + 1, index, row2)
                                resultWritten2 += 1
                                print(f'Process 2: {resultWritten2}')
                                break
                            except StopIteration:
                                if firstBreak > LIMIT:
                                    firstBreak = 0
                                    break
                                firstBreak += 1
                                searchDate2 = searchDate2 + relativedelta(days=-1)
                                pass
        except:
            print(sys.exc_info())
        finally:
            excel_helper.close()
    else:
        print('Invalid Action')

# SELECT
#     CAST ( [date] AS DATE ) as 'Date',
#     [open] as 'Open',
#     MAX(high) AS High,
#     MIN(low) AS Low,
# 		Sum(volume) as Volume,
#     [close] as 'Close'
# FROM
# (
#     SELECT
# 			FIRST_VALUE ( [open] ) OVER ( PARTITION BY CAST ( [date] AS DATE ) ORDER BY [time] ) AS [open],
# 		FIRST_VALUE ( [close] ) OVER ( PARTITION BY CAST ( [date] AS DATE ) ORDER BY [time] DESC ) AS [close],
# 	[date],
# 	low,
# 	high,
# 	volume
# FROM
# 	stocks
# 		WHERE stock = 'AAL' And [time] >= 900 And [time] <= 905
# ) as T
# GROUP BY CAST ( [date] AS DATE ), [open], [close]
# ORDER BY CAST ( [date] AS DATE )
