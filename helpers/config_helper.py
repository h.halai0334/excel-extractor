import json


class ConfigHelper:
    __database: str = ''
    __server: str = ''
    __user_name: str = ''
    __password: str = ''
    __driver: str = ''
    __connection_string: str = ''

    @property
    def database(self):
        return self.__database

    @database.setter
    def database(self, value):
        self.__database = value
        pass

    @property
    def server(self):
        return self.__server

    @server.setter
    def server(self, value):
        self.__server = value

    @property
    def user_name(self):
        return self.__user_name

    @user_name.setter
    def user_name(self, value):
        self.__user_name = value

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, value):
        self.__password = value

    @property
    def driver(self):
        return self.__driver

    @driver.setter
    def driver(self, value):
        self.__driver = value

    @property
    def connection_string(self):
        return self.__connection_string

    @connection_string.setter
    def connection_string(self, value):
        self.__connection_string = value
        pass

    def read_config(self):
        with open('config.json') as f:
            config = json.load(f)
            self.database = config['database']
            self.password = config['password']
            self.driver = config['driver']
            self.user_name = config['user_name']
            self.server = config['server']
            self.connection_string = f'mssql://{self.user_name}:{self.password}@{self.server}' \
                                     f'/{self.database}?driver={self.driver}'
            f.close()
        pass

    def save_config(self):
        with open('config.json', 'w') as f:
            json.dump(self, f)
            f.close()
