import openpyxl
import xlrd

from timeit import default_timer as timer


class ExcelHelper:
    excel_work_book = None
    excel_sheets = []
    thread_id = None

    def get_active_instance(self, file: str):
        print('Loading file Please wait....')
        start = timer()
        self.excel_work_book = openpyxl.load_workbook(filename=file, read_only=False, keep_vba=True)
        self.excel_sheets = {sheet.title: sheet for sheet in self.excel_work_book.worksheets}
        end = timer()
        print(f'File Loaded: {end - start} Seconds')
        pass

    def get_cell_value(self, sheet: int, cell_row: int, cell_column: int):
        return self.excel_sheets[sheet].cell(row=cell_row, column=cell_column).value
        pass

    def get_cell_value_by_name(self, sheet: str, cell_row: int, cell_column: int):
        return self.excel_sheets[sheet].cell(row=cell_row, column=cell_column).value
        pass

    def get_cell_value_by_name_range(self, sheet: str, cell_range_a: str, cell_range_b: str):
        range_values = self.excel_sheets[sheet][f"{cell_range_a}:{cell_range_b}"]
        range_values = [cell[0].value for cell in range_values]
        return range_values
        pass

    def get_cell_value_by_name_range_vertical(self, sheet: str, cell_range_a: str, cell_range_b: str):
        range_values = self.excel_sheets[sheet][f"{cell_range_a}:{cell_range_b}"]
        range_values = [cell[0].value for cell in range_values]
        return range_values
        pass

    def get_cell_value_by_name_range_horizontal(self, sheet: str, cell_range_a: str, cell_range_b: str):
        range_values = self.excel_sheets[sheet][f"{cell_range_a}:{cell_range_b}"]
        range_values = [cell.value for cell in range_values[0]]
        return range_values
        pass

    def write_to_cell_by_name(self, sheet: str, cell_row: int, cell_column: int, value: str):
        self.excel_sheets[sheet].cell(row=cell_row, column=cell_column).value = value
        pass

    def write_to_cell(self, sheet: int, cell_row: int, cell_column: int, value: str):
        self.excel_sheets[sheet].cell(row=cell_row, column=cell_column).value = value
        pass

    def write_stock_data(self, index: int, cell_row: int, cell_column: int, value: dict):
        self.excel_sheets['Open' + str(index)].cell(row=cell_row, column=cell_column).value = value[
            "Open"]
        self.excel_sheets['Close' + str(index)].cell(row=cell_row, column=cell_column).value = value[
            "Close"]
        self.excel_sheets['Low' + str(index)].cell(row=cell_row, column=cell_column).value = value["Low"]
        self.excel_sheets['Volume' + str(index)].cell(row=cell_row, column=cell_column).value = value[
            "Volume"]
        self.excel_sheets['High' + str(index)].cell(row=cell_row, column=cell_column).value = value[
            "High"]
        pass

    def save(self, file: str):
        self.excel_work_book.save(file)
        pass

    def close(self):
        self.excel_work_book = None
        pass

    def column_string(self, n: int):
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        return string

    def print_error(self, number: int):
        e_msg = win32api.FormatMessage(number)
        print(e_msg)
