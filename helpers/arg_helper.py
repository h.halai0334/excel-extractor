import getopt
import sys

import constants.option_constants as option


class ArgumentHelper:

    def __init__(self):
        self.directory = ""
        self.action = ""
        self.start1 = ""
        self.start2 = ""
        self.end1 = ""
        self.end2 = ""

    def check_args(self, argv):
        try:
            opts, args = getopt.getopt(argv, option.ALL_ARG_SHORT, option.ALL_ARG_LONG)
        except getopt.GetoptError:
            print('Error in Options. Please do a -h to get more options')
            print(sys.exc_info())
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print(f"test.py -d|--direction= <directory> -a|--action= <action('load','import')>"
                      f" --start1=<starttime('9:30')> --end1=<endtime('9:30')>"
                      f" --start2=<starttime2('9:30')> --end2=<endtime2('9:30')>"
                      f"")
                sys.exit()
            elif opt in (option.ACTION_ARG_SHORT, option.ACTION_ARG_LONG):
                self.action = arg
                pass
            elif opt in (option.DIRECTORY_ARG_SHORT, option.DIRECTORY_ARG_LONG):
                self.directory = arg
                pass
            elif opt in option.START_1_ARG_LONG:
                self.start1 = arg
                pass
            elif opt in option.START_2_ARG_LONG:
                self.start2 = arg
                pass
            elif opt in option.END_1_ARG_LONG:
                self.end1 = arg
                pass
            elif opt in option.END_2_ARG_LONG:
                self.end2 = arg
                pass
        pass
