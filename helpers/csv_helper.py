import csv
import ntpath
import datetime

from models.stock import Stock


class CsvHelper:
    def read_all_rows(self, path):
        list_items = []
        name = ntpath.basename(path).split('_')[0]
        with open(path, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)
            for row in csv_reader:
                try:
                    time_obj = datetime.datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").time()
                    date_obj = datetime.datetime.strptime(row[0], "%Y-%m-%d %H:%M:%S").date()
                    if date_obj.year < 2009:
                        continue
                    minute = time_obj.minute
                    hr = time_obj.hour
                    total_min = (hr * 60) + minute
                    model = {"date": row[0],
                             "time": total_min,
                             "day": date_obj.timetuple().tm_yday,
                             "volume": row[5],
                             "open": row[1], "high": row[2], "low": row[3], "close": row[4],
                             "stock": name}
                    list_items.append(model)
                except IndexError:
                    continue
        pass
        return list_items
